# specify compiler
GXX=clang++ 
CFLAGS = -g -Wfatal-errors -std=c++11

all: 
	$(info Make <target> ) 
	$(info Possible targets : day1 | day1b )
	$(info _________________  day2 | day2b )
	$(info _________________  day3 )
	$(info _________________  day4 )
     
day1: ./day_1/day1.cpp
	$(GXX) $(CFLAGS) -o day1.out ./day_1/day1.cpp
				
day1b: ./day_1/day1b.cpp
	$(GXX) $(CFLAGS) -o day1b.out ./day_1/day1b.cpp
								
day2: ./day_2/day2.cpp
	$(GXX) $(CFLAGS) -o day2.out ./day_2/day2.cpp
												
day2b: ./day_2/day2b.cpp
	$(GXX) $(CFLAGS) -o day2b.out ./day_2/day2b.cpp
																
day3: ./day_3/day3.cpp
	$(GXX) $(CFLAGS) -o day3.out ./day_3/day3.cpp
																				
day4: ./day_4/day4.cpp
	$(GXX) $(CFLAGS) -o day4.out ./day_4/day4.cpp

day5: ./day_5/day5.cpp
	$(GXX) $(CFLAGS) -o day5.out ./day_5/day5.cpp

day6: ./day_6/day6.cpp
	$(GXX) $(CFLAGS) -o day6.out ./day_6/day6.cpp

day7: ./day_7/day7.cpp
	$(GXX) $(CFLAGS) -o day7.out ./day_7/day7.cpp

day8: ./day_8/day8.cpp
	$(GXX) $(CFLAGS) -o day8.out ./day_8/day8.cpp

clean: 
	rm *.out



