#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>

int main() {
  std::ifstream infile("input.txt");
  std::string str;
  int res = 0;
  while (std::getline( infile, str))
  {
    std::istringstream ss(str);
    std::istream_iterator<std::string> begin(ss), end;
    std::vector<std::string> arrayTokens(begin, end);
    for ( int i = 0; i < arrayTokens.size(); i++)
    {
      for ( int j = i+1; j < arrayTokens.size(); j++)
      {
        int ii = std::stoi(arrayTokens[i]);
        int jj = std::stoi(arrayTokens[j]);
        if ((ii % jj) == 0)
        {
          std::cout << ii << "/" << jj << std::endl;
          res += (ii / jj);
          break;
        }
        if ((jj % ii) == 0)
        {
          std::cout << jj << "/" << ii << std::endl;
          res += (jj / ii);
          break;
        }
      }
    }
    
  }
  std::cout << "Result is " << res << '\n';
}
