#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>

int main() {
  std::ifstream infile("input.txt");
  std::string str;
  int res = 0;
  while (std::getline( infile, str))
  {
    std::istringstream ss(str);
    std::istream_iterator<std::string> begin(ss), end;
    std::vector<std::string> arrayTokens(begin, end);
    int min = -1;
    int max = -1;
    for ( int i = 0; i < arrayTokens.size(); i++)
    {
      if (max == -1 || max < std::stoi(arrayTokens[i])) max = std::stoi(arrayTokens[i]);
      if (min == -1 || min > std::stoi(arrayTokens[i])) min = std::stoi(arrayTokens[i]);
    }
    res +=std::abs(max - min);
  }
  std::cout << "Result is " << res << '\n';
}
