#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

int main() {
  std::ifstream infile("input.txt");
  std::string str;
  getline( infile, str);
  int result = 0;
  for(std::string::size_type i = 0; i < str.size(); ++i) {
    if (str[i] == str[(i+1)%str.size()])
      result += (int)(str[i] - '0');
  }
  std::cout << "Result: " << result << "\n";
}
