#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <iterator>


int jumps(int offset, std::vector<int>& trampolines) {
	int ptr = 0;
	int jumps = 0;
	while (ptr < trampolines.size() && ptr > -1) {
		int oldptr = ptr;
		ptr += trampolines[ptr];
		jumps++;
		trampolines[oldptr] += trampolines[oldptr] >= 3 ? offset : 1;
	}
	return jumps;
}

int main() {
	std::vector<int> trampolines {std::istream_iterator<int>{std::cin}, {}};
	std::vector<int> trampolinesb(trampolines);
	std::cout << "Solution a: " << jumps( 1, trampolines) << std::endl;
	std::cout << "Solution b: " << jumps(-1, trampolinesb) << std::endl;
}


